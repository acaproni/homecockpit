
print("ACA - Generic Panel control started...")

-- Initialization
print("ACA - disabling zoom on mouse wheel")
do_on_mouse_wheel("RESUME_MOUSE_WHEEL = true")

DataRef("isZibo", "sim/aircraft/view/acf_tailnum", "readonly",0)
print(string.format("Plane ICAO = %s, is zibo=%s",PLANE_ICAO,isZibo))

if (PLANE_ICAO == "B738" and isZibo=="ZB738") then
  print(string.format("ACA - Generic panerl: recognized plane %s",PLANE_ICAO))

  eng1HeatCmd = "laminar/B738/toggle_switch/eng1_heat"
  eng2HeatCmd = "laminar/B738/toggle_switch/eng2_heat"
  wingHeatCmd = "laminar/B738/toggle_switch/wing_heat"
  reverseCmd = "sim/engines/thrust_reverse_toggle"




  -- CUTOFF/IDLE leftand right button
  cutoffLeftBtn = 809
  cutoffLeftDR = "laminar/B738/engine/mixture_ratio1"
  create_switch(cutoffLeftBtn,cutoffLeftDR,0,0,1)
  cutoffRightBtn = 814
  cutoffRightDR = "laminar/B738/engine/mixture_ratio2"
  create_switch(cutoffRightBtn,cutoffRightDR,0,0,1)
  --[[
  function syncCutoffs()
    leftCutoffState = button(cutoffLeftBtn)
    rightCutoffState =button(cutoffRightBtn)
  end

  -- sync all switches
  function syncGenericPnlSwitches()
    syncCutoffs()
  end

  -- Update the state of the switches in the ZIBO panel aprox once per second
  do_often("syncGenericPnlSwitches()")
  ]]
end
