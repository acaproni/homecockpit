--[[
  A FlyWithLua script to control light switch connected to a LeoBodnar
  card to the ZIBo B738.

  This script does not manage the 2 engine start rotaries because
  they can be controlled directly by XP associating the events to the joy buttons
  provided by the LeoBodnar

  It supports:
    * retractbale and fixed landing LightSwitchZiboPos
    * runaway turnoff lights
    * taxi
    * APU
    * ignition selector
    * Logo
    * Position/strobe
    * Anti collision (red beacon)
    * Wing
    * Wheel well

    To adapt use the FWL macro to get the number of joy button when you press
    the switches in the panel and replace the value in the script
    (i.e. the numbers in the variables whose name ends with Btn
    like APUStartBtn)

    At each iteration the script read the position of the switches in the
    LeoBodnar and move the switch in the ZIBO.
    The iteration runs once per second to reduce impact on performance.

    Note: at each iteration the panel moves each switch in the ZIBO *one step*
          towards the direction selected in the LeoBodnar
          This means that if you move the switch of 2 positions then the Script
          needs 2 steps to sync the ZIBO
          For example if you move both retractable landing lights from OFF (up)
          to ON (down) then at the first iteration both switches move
          to extend (middle) and at the second iteration both switches
          move to ON (down)

   APU is a On-OFF-MON switch. To start keep the button in the START position
   for few seconds

   Note 2: the script disable the mouse wheel (i.e. no noisy zomm but also
           cannot use the mouse wheel for the rotary knobs in the ZIBO
           This is fine with a complete home cockpit IMO.

           TO ENABLE the mouse wheel comment the line starting with
           do_on_mouse_wheel with --
]]

logMsg("ACA - Light Panel control started...")

DataRef("isZibo", "sim/aircraft/view/acf_tailnum", "readonly",0)
logMsg(string.format("Plane ICAO = %s, is zibo=%s",PLANE_ICAO,isZibo))

if (PLANE_ICAO == "B738" and isZibo=="ZB738") then
  logMsg(string.format("Recognized plane %s",PLANE_ICAO))

  -- This function setup a ON/OFF/ON switch controlled
  -- by a XP command
  --
  -- upBtnNr: the number of the toggle of the joy for the UP position
  -- dnBtnNr: the number of the toggle of the joy for the UP position
  -- upCmd: the command to move the switch up in the ZIBO
  -- dnCmd: the command to move the switch dn in the ZIBO
  -- btnState (dataref) the position of the switch in the ZIBO (0=ON, 1=OFF, 2=ON)
  -- upValue the value assumed by btnState in the up position
  -- midValue the value assumed by btnState in the mid (centered) position
  -- dnValue the value assumed by btnState in the down position
  function syncBtn(
      upBtnNr,
      dnBtnNr,
      upCmd,
      dnCmd,
      btnState,
      upValue,
      midValue,
      dnValue)
    upBtnState = button(upBtnNr)
    dnBtnState = button(dnBtnNr)
    if (upBtnState==true) then
      target = upValue
    elseif (dnBtnState==true) then
      target = dnValue
    else
      target = midValue
    end
    if (target<btnState) then
      command_once(upCmd)
    elseif (target>btnState) then
      command_once(dnCmd)
    end
  end

  -- APU
  ApuBtnDnCmd ="laminar/B738/spring_toggle_switch/APU_start_pos_up"
  ApuBtnUpCmd ="laminar/B738/spring_toggle_switch/APU_start_pos_dn"

  -- The position of the switch in the panel
  ApuSwitchPos="laminar/B738/spring_toggle_switch/APU_start_pos"
  DataRef("ApuSwitch",ApuSwitchPos,"readonly")

  APUStartBtn = 642
  APUOffBtn = 644

  -- Move the APU button in the ZIBO to match the state of that
  -- of the buttons in the HW panel
  --
  -- syncBtn(..) can't be used because we need to issue a start/end of the
  -- command to start the APU
  function moveApu()
      apuOffBtnState = button(APUOffBtn)
      apuStartBtnState = button(APUStartBtn)

      if (apuOffBtnState==true) then
        apuTarget = 0
      elseif (apuStartBtnState==true) then
        apuTarget = 2
      else
        apuTarget = 1
      end
      --draw_string(20, SCREEN_HIGHT - 1270, string.format("APU target=%i pos=%i",apuTarget,ApuSwitch), "red")
      if (apuTarget<ApuSwitch) then
        if (ApuSwitch==2) then
          command_end(ApuBtnUpCmd)
        else
          command_once(ApuBtnDnCmd)
        end
      elseif (apuTarget>ApuSwitch) then
        if (apuTarget==2) then
          command_begin(ApuBtnUpCmd)
        else
          command_once(ApuBtnUpCmd)
        end
      end

  end

  -- Taxi Light
  TaxiLightBtn = 640
  TaxiLightCmd = "laminar/B738/toggle_switch/taxi_light_brigh_toggle"
  DataRef("LightSwitchZiboPos","laminar/B738/toggle_switch/taxi_light_brightness_pos","readonly",0)

  function moveTaxi()
    taxiJoyPos = button(TaxiLightBtn)
    if ((taxiJoyPos==true and LightSwitchZiboPos==0) or (taxiJoyPos==false and LightSwitchZiboPos==2)) then
      command_once(TaxiLightCmd)
    end
  end

  -- Fixed landing light switches
  FixedLandingRightBtn = 654
  FixedLandingLefttBtn = 651
  create_switch(FixedLandingRightBtn,"laminar/B738/switch/land_lights_right_pos",0,0,1)
  create_switch(FixedLandingLefttBtn,"laminar/B738/switch/land_lights_left_pos",0,0,1)

  -- Retractable landing light switches
  RetLandingRightUpBtn = 646
  RetLandingRightDnBtn = 647
  RetLandingLeftUpBtn = 641
  RetLandingLeftDnBtn = 643
  RetLandingLightLeftDnCmd="laminar/B738/switch/land_lights_ret_left_dn"
  RetLandingLightLeftUpCmd="laminar/B738/switch/land_lights_ret_left_up"
  DataRef("LeftRetSwitchZiboPos","laminar/B738/switch/land_lights_ret_left_pos","readonly",0)
  RetLandingLightRightDnCmd="laminar/B738/switch/land_lights_ret_right_dn"
  RetLandingLightUpRightCmd="laminar/B738/switch/land_lights_ret_right_up"
  DataRef("RightRetSwitchZiboPos","laminar/B738/switch/land_lights_ret_right_pos","readonly",0)

  function moveRetLandingLight()
    -- Right
    syncBtn(
      RetLandingRightUpBtn,
      RetLandingRightDnBtn,
      RetLandingLightUpRightCmd,
      RetLandingLightRightDnCmd,
      RightRetSwitchZiboPos,
      0,
      1,
      2)
    -- Left
    syncBtn(
      RetLandingLeftUpBtn,
      RetLandingLeftDnBtn,
      RetLandingLightLeftUpCmd,
      RetLandingLightLeftDnCmd,
      LeftRetSwitchZiboPos,
      0,
      1,
      2)
  end

  -- Engine start
  LeftIgnBtn = 660
  RightIgnBtn = 657

  IgnLeftCmd = "laminar/B738/toggle_switch/eng_start_source_left"
  IgnRightCmd = "laminar/B738/toggle_switch/eng_start_source_right"
  DataRef("EngStartZiboPos","laminar/B738/toggle_switch/eng_start_source","readonly",0)

  function moveEngStart()
    syncBtn(
      LeftIgnBtn,
      RightIgnBtn,
      IgnLeftCmd,
      IgnRightCmd,
      EngStartZiboPos,
      -1,
      0,
      1)
  end

  -- Logo Light
  LogoLightBtn = 669
  LogoLightCmdOn  = "laminar/B738/switch/logo_light_on" --	Logo Light Switch On
  LogoLightCmdOff = "laminar/B738/switch/logo_light_off" -- Logo Light Switch Off
  function moveLogo()
    logoJoyPos = button(LogoLightBtn)
    if (logoJoyPos) then
      command_once(LogoLightCmdOn)
    else
      command_once(LogoLightCmdOff)
    end
  end

  -- Strobe/Steady Light
  StrobeBtn = 666
  StrobeUpCmd = "laminar/B738/toggle_switch/position_light_up"
  SteadyBtn = 667
  SteadyDnCmd = "laminar/B738/toggle_switch/position_light_down"
  DataRef("PosLightZiboPoos", "laminar/B738/toggle_switch/position_light_pos","readonly",0)

  function moveStrobeLight()
    syncBtn(
    SteadyBtn,
    StrobeBtn,
    SteadyDnCmd,
    StrobeUpCmd,
    PosLightZiboPoos,
    -1,
    0,
    1)
  end

  -- Anti-collision (beacon)
  BeaconBtn = 664
  BeaconDataRef = "sim/cockpit2/switches/beacon_on"
  create_switch(BeaconBtn,BeaconDataRef,0,0,1)

  -- Wing and Wheel well are part of an array of a DataRef
  WingBtn = 659
  WheelBtn = 663
  WingCmdOn = "laminar/B738/switch/wing_light_on"
  WingCmdOff = "laminar/B738/switch/wing_light_off"
  WheelCmdOn = "laminar/B738/switch/wheel_light_on"
  WheelCmdOff = "laminar/B738/switch/wheel_light_off"

  function moveWing()
    wingJoyPos = button(WingBtn)
    if (wingJoyPos) then
      command_once(WingCmdOn)
    else
      command_once(WingCmdOff)
    end
  end

  function moveWheel()
    wheelJoyPos = button(WheelBtn)
    if (wheelJoyPos) then -- Reversed?
      command_once(WheelCmdOff)
    else
      command_once(WheelCmdOn)
    end
  end

  -- Left and Right turnoff
  -- Uses the same generic dataref of Wing and Wheel well
  --
  -- I have not found a command to sync the state in the ZIBO Panel
  -- but the buttons in the joy turns on and off the lights (i.e. they work well)
  LeftTurnoffBtn  = 655
  RightTurnoffBtn = 648
  LeftRwyCmdOn = "laminar/B738/switch/rwy_light_left_on" --   Runway light left on
  LeftRwyCmdOff = "laminar/B738/switch/rwy_light_left_off" --	 Runway light left off
  RightRwyCmdOn = "laminar/B738/switch/rwy_light_right_on" --	 Runway light right on
  RightRwyCmdOff = "laminar/B738/switch/rwy_light_right_off" -- Runway light right off

  function moveRwyLights()
    leftRwyBtnPos  = button(LeftTurnoffBtn)
    rightRwyBtnPos = button(RightTurnoffBtn)
    if (leftRwyBtnPos) then
      command_once(LeftRwyCmdOn)
    else
      command_once(LeftRwyCmdOff)
    end
    if (rightRwyBtnPos) then
      command_once(RightRwyCmdOn)
    else
      command_once(RightRwyCmdOff)
    end
  end

  Starter1CmdLeft  = "laminar/B738/knob/eng1_start_left" --Engine starter 1 left
  Starter1CmdRight = "laminar/B738/knob/eng1_start_right" --Engine starter 1 right
  Starter2CmdLeft  = "laminar/B738/knob/eng2_start_left" --Engine starter 2 left
  Starter2CmdRight = "laminar/B738/knob/eng2_start_right" --Engine starter 2 right

  Starter1CmdGrd  = "laminar/B738/rotary/eng1_start_grd" --ENGINE STARTER1 GRD
  Starter1CmdOff  = "laminar/B738/rotary/eng1_start_off" --ENGINE STARTER1 OFF
  Starter1CmdCont = "laminar/B738/rotary/eng1_start_cont" --ENGINE STARTER1 CONT
  Starter1CmdFlt  = "laminar/B738/rotary/eng1_start_flt" --ENGINE STARTER1 FLT
  Starter2CmdGrd  = "laminar/B738/rotary/eng2_start_grd" --ENGINE STARTER2 GRD
  Starter2CmdOff  = "laminar/B738/rotary/eng2_start_off" --ENGINE STARTER2 OFF
  Starter2CmdCont = "laminar/B738/rotary/eng2_start_cont" --ENGINE STARTER2 CONT
  Starter2CmdFlt  = "laminar/B738/rotary/eng2_start_flt" --ENGINE STARTER2 FLT

  Starter1GrdBtn = 645
  Starter1OffBtn = 670
  Starter1ContBtn = 652
  Starter1FltBtn = 653

  Starter2GrdBtn = 661
  Starter2OffBtn = 665
  Starter2ContBtn = 662
  Starter2FltBtn = 668

  -- POSITIONS: 0 GRD, 1 OFF, 2 CONT, 3 FLT
  DataRef("EngStarter1Pos","laminar/B738/engine/starter1_pos","readonly",0)
  DataRef("EngStarter2Pos","laminar/B738/engine/starter2_pos","readonly",0)


  function moveEngStarter(grdBtn, offBtn, contBtn, fltBtn, panelPosRef, grdCmd, offCmd, contCmd, fltCmd)
    grd  = 0
    off  = 1
    cont = 2
    flt  = 3

    grdPressed  = button(grdBtn)
    offPressed  = button(offBtn)
    contPressed = button(contBtn)
    fltPressed  = button(fltBtn)
    panelPos = panelPosRef

    if (grdPressed) then
      requested = grd
    elseif (offPressed) then
      requested = off
    elseif (contPressed) then
      requested = cont
    elseif (fltPressed) then
      requested = flt
    else
      requested = -1 -- Should never happen
    end


    if (not (requested==-1) and not (requested==panelPos)) then
      if (requested==off) then
        cmd=offCmd
      elseif (requested==cont) then
        cmd=contCmd
      elseif (requested==flt) then
        cmd=fltCmd
      elseif (requested==grd) then
        cmd=grdCmd
      end

      if (not (cmd==nil) and not (cmd=='')) then
        logMsg(string.format("ACA light panel: moving starter swithch to pos %d (%s)",requested,cmd))
        command_once(cmd)
      end
    end
  end

  function moveStarters()
    moveEngStarter(
      Starter1GrdBtn, Starter1OffBtn, Starter1ContBtn, Starter1FltBtn, EngStarter1Pos,
      Starter1CmdGrd, Starter1CmdOff, Starter1CmdCont, Starter1CmdFlt)
      moveEngStarter(
        Starter2GrdBtn, Starter2OffBtn, Starter2ContBtn, Starter2FltBtn, EngStarter2Pos,
        Starter2CmdGrd, Starter2CmdOff, Starter2CmdCont, Starter2CmdFlt)
  end


  function refreschSwitches()
    moveTaxi()
    moveApu()
    moveRetLandingLight()
    moveEngStart()
    moveStrobeLight()
    moveWing()
    moveWheel()
    moveRwyLights()
    moveLogo()
    moveStarters()
  end

  -- Update the state of the switches in the ZIBO panel aprox once per second
  do_often("refreschSwitches()")

end
